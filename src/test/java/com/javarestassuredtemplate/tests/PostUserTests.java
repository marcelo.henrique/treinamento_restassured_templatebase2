package com.javarestassuredtemplate.tests;

import com.javarestassuredtemplate.bases.TestBase;
import com.javarestassuredtemplate.jsonObjects.User;
import com.javarestassuredtemplate.requests.PostUserRequest;
import io.restassured.response.ValidatableResponse;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;
import static org.hamcrest.Matchers.equalTo;

import java.util.Random;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class PostUserTests extends TestBase {
    PostUserRequest postUserRequest;

    @Test
    public void inserirUsuarioComTodosDadosValidos(){

        //Parâmetros
        int id = 9999;
        String username = "marcelohenrique";
        String firstName = "Marcelo";
        String lastName = "Henrique";
        String email = "marcelohenriquees@gmail.com";
        String password = "teste123";
        String phone = "37991682156";
        int userStatus = 0;
        int statusCodeEsperado = HttpStatus.SC_OK;

        //Fluxo
        postUserRequest = new PostUserRequest();
        postUserRequest.setJsonBodyUsingJsonFile(id, username, firstName, lastName, email, password, phone, userStatus);
        ValidatableResponse response = postUserRequest.executeRequest();

        //Asserções
        response.statusCode(statusCodeEsperado);
        response.body("id",equalTo(id),
                "username", equalTo(username),
                "firstName", equalTo(firstName),
                "lastName", equalTo(lastName),
                "email", equalTo(email),
                "password", equalTo(password),
                "phone", equalTo(phone),
                "userStatus", equalTo(userStatus));
    }

    @Test(dataProvider = "dataUserObjectProvider",dataProviderClass=User.class)
    public void inserirUsuarioComTodosDadosValidosUsandoJavaObjectNoTeste(User dataUserObjectProvider){
        //Parâmetros
        int id = dataUserObjectProvider.getId();
        String username = dataUserObjectProvider.getUsername();
        String firstName = dataUserObjectProvider.getFirstName();
        String lastName = dataUserObjectProvider.getLastName();
        String email = dataUserObjectProvider.getEmail();
        String password = dataUserObjectProvider.getPassword();
        String phone = dataUserObjectProvider.getPhone();
        int userStatus = dataUserObjectProvider.getUserStatus();
        int statusCodeEsperado = HttpStatus.SC_OK;
        /*given().
                baseUri("https://petstore.swagger.io").
                header("content-type", "application/json").
                basePath("/v2").
                body(dataUserObjectProvider).
                when().
                post("/user").
                then().
                log().all().
                assertThat().
                body("id",equalTo(dataUserObjectProvider.getId()),
                        "username", equalTo(dataUserObjectProvider.getUsername()),
                        "firstName", equalTo(dataUserObjectProvider.getFirstName()),
                        "lastName", equalTo(dataUserObjectProvider.getLastName()),
                        "email", equalTo(dataUserObjectProvider.getEmail()),
                        "password", equalTo(dataUserObjectProvider.getPassword()),
                        "phone", equalTo(dataUserObjectProvider.getPhone()),
                        "userStatus", equalTo(dataUserObjectProvider.getUserStatus()));*/

        //Serialização do Json em um objeto java
        User user = new User();
        user.setId(id);
        user.setUsername(username);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);
        user.setPassword(password);
        user.setPhone(phone);
        user.setUserStatus(userStatus);

        //Fluxo
        postUserRequest = new PostUserRequest();
        postUserRequest.setJsonBodyUsingJavaObject(user);
        ValidatableResponse response = postUserRequest.executeRequest();

        //Asserções
        response.statusCode(statusCodeEsperado);
        response.body("id",equalTo(id),
                "username", equalTo(username),
                "firstName", equalTo(firstName),
                "lastName", equalTo(lastName),
                "email", equalTo(email),
                "password", equalTo(password),
                "phone", equalTo(phone),
                "userStatus", equalTo(userStatus));
    }
}
