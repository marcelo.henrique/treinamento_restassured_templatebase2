package com.javarestassuredtemplate.tests;

import com.javarestassuredtemplate.bases.TestBase;
import com.javarestassuredtemplate.jsonObjects.User;
import com.javarestassuredtemplate.requests.GetUserRequest;
import io.restassured.response.ValidatableResponse;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;
import java.util.Random;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class GetUserTests extends TestBase {
    GetUserRequest getUserRequest;

    @Test(dataProvider = "dataUserObjectProvider",dataProviderClass=User.class)
    public void buscarUsuarioExistente(User dataUserObjectProvider){
        //Parâmetros (Arrange)
        String username = dataUserObjectProvider.getUsername();
        int statusCodeEsperado = HttpStatus.SC_OK;
        
        /*given().
                baseUri(baseURI).
                basePath("/user/{username}").
                pathParam("username", dataUserObjectProvider.getUsername()).
                when().
                get().
                then().
                statusCode(200).
                body("id",equalTo(dataUserObjectProvider.getId()),
                        "username", equalTo(dataUserObjectProvider.getUsername()),
                        "firstName", equalTo(dataUserObjectProvider.getFirstName()),
                        "lastName", equalTo(dataUserObjectProvider.getLastName()),
                        "email", equalTo(dataUserObjectProvider.getEmail()),
                        "password", equalTo(dataUserObjectProvider.getPassword()),
                        "phone", equalTo(dataUserObjectProvider.getPhone()),
                        "userStatus", equalTo(dataUserObjectProvider.getUserStatus()));*/


        //Fluxo (Acts)
        getUserRequest = new GetUserRequest(username);
        ValidatableResponse response = getUserRequest.executeRequest();

        //Asserções (Assert)
        response.statusCode(statusCodeEsperado);
        response.body("id",equalTo(dataUserObjectProvider.getId()),
                        "username", equalTo(dataUserObjectProvider.getUsername()),
                        "firstName", equalTo(dataUserObjectProvider.getFirstName()),
                        "lastName", equalTo(dataUserObjectProvider.getLastName()),
                        "email", equalTo(dataUserObjectProvider.getEmail()),
                        "password", equalTo(dataUserObjectProvider.getPassword()),
                        "phone", equalTo(dataUserObjectProvider.getPhone()),
                        "userStatus", equalTo(dataUserObjectProvider.getUserStatus()));
    }

}
