package com.javarestassuredtemplate.requests;

import com.javarestassuredtemplate.bases.RequestRestBase;
import io.restassured.http.Method;

public class GetUserRequest extends RequestRestBase {
    public GetUserRequest(String username) {
        requestService = "/user/" + username;
        method = Method.GET;
    }
}
