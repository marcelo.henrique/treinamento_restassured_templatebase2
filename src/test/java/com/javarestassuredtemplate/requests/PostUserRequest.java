package com.javarestassuredtemplate.requests;

import com.javarestassuredtemplate.bases.RequestRestBase;
import com.javarestassuredtemplate.jsonObjects.Tag;
import com.javarestassuredtemplate.jsonObjects.User;
import com.javarestassuredtemplate.utils.GeneralUtils;
import io.restassured.http.Method;

public class PostUserRequest extends RequestRestBase {
    public PostUserRequest(){
        requestService = "/user";
        method = Method.POST;
    }

    public void setJsonBodyUsingJsonFile(int id,
                                         String username,
                                         String firstName,
                                         String lastName,
                                         String email,
                                         String password,
                                         String phone,
                                         int userStatus){
        jsonBody = GeneralUtils.readFileToAString("src/test/java/com/javarestassuredtemplate/jsons/PostUserJson.json")
                .replace("$id", String.valueOf(id))
                .replace("$username", String.valueOf(username))
                .replace("$firstName", String.valueOf(firstName))
                .replace("$lastName", String.valueOf(lastName))
                .replace("$email", String.valueOf(email))
                .replace("$password", String.valueOf(password))
                .replace("$phone", String.valueOf(phone))
                .replace("$userStatus", String.valueOf(userStatus));
    }

    public void setJsonBodyUsingJavaObject(int id,
                                           String username,
                                           String firstName,
                                           String lastName,
                                           String email,
                                           String password,
                                           String phone,
                                           int userStatus){
        jsonBody = new User(id,
                username,
                firstName,
                lastName,
                email,
                password,
                phone,
                userStatus);
    }

    public void setJsonBodyUsingJavaObject(Object jsonObject){
        jsonBody = jsonObject;
    }
}
