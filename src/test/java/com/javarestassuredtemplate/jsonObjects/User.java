package com.javarestassuredtemplate.jsonObjects;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.testng.annotations.DataProvider;

import java.util.Random;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class User {
    private int id;
    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String phone;
    private int userStatus;




    @DataProvider(name = "dataUserObjectProvider")
    public  Object[] dataUserProvider() {
        Random random = new Random();

        //Usuario1
        User user1 = new User();
        user1.setId(random.nextInt());
        user1.setUsername("marcelohenrique");
        user1.setFirstName("Marcelo");
        user1.setLastName("Henrique");
        user1.setEmail("marcelohenriquees@gmail.com");
        user1.setPassword("teste123");
        user1.setPhone("37991682156");
        user1.setUserStatus(0);

        //Usuario2
        User user2 = new User();
        user2.setId(random.nextInt());
        user2.setUsername("joao_alves");
        user2.setFirstName("joaO");
        user2.setLastName("Alves");
        user2.setEmail("joao.alves@gmail.com");
        user2.setPassword("Joao5456");
        user2.setPhone("5487451");
        user2.setUserStatus(1);

        //Usuario3
        User user3 = new User();
        user3.setId(random.nextInt());
        user3.setUsername("lauane.lauane");
        user3.setFirstName("lAuAne");
        user3.setLastName("S0ouza");
        user3.setEmail("lauane_d@outlook.com");
        user3.setPassword("lau@S0uz4");
        user3.setPhone("4848481");
        user3.setUserStatus(400);

        //Usuario4
        User user4 = new User();
        user4.setId(random.nextInt());
        user4.setUsername("larissa-oliveira");
        user4.setFirstName("Laris sa");
        user4.setLastName("Oli veira");
        user4.setEmail("lara_1@gmail.com");
        user4.setPassword("1$#%%@22%O");
        user4.setPhone("(37) 9158-7485");
        user4.setUserStatus(43243);

        return new User[]{user1,user2,user3,user4};
    }

}

